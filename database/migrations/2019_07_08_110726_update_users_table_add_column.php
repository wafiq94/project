<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
			$table->integer('department_id')->unsigned()->index()->nullable();
			$table->foreign('department_id')->references('id')->on('department');
		});
    }

   
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
